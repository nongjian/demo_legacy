package com.zhuxm.util;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.zhuxm.user.entity.User;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.Map;

/**
 * Create by XiaoMo
 * Date : 2019/5/6
 * Time : 16:31
 **/

/**
 * token签发工具类
 */
public class JwtUtil {

    //秘钥
    private static final String TOKEN_SECRET = "lskjdfokjo2kfjdsokfj123123";
    private static final String TOKEN_ISSUER = "Zhuxm";

    @Data
    @Accessors(chain = true)
    public static class TokenData {
        private Boolean success;
        private String reason;
        private Date time;
        private JSONObject data;
    }

    /**
     * 生成token
     *
     * @param json
     * @return
     */
    public static String encodeToken(String json, Long expireTime) {
        JWTCreator.Builder builder = JWT.create();
        builder = builder.withIssuer(TOKEN_ISSUER);
        Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
        if (expireTime != null && expireTime > 0) {
            Date date = new Date(System.currentTimeMillis() + expireTime);
            builder = builder.withExpiresAt(date);
        } else {
            Date date = new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 24 * 30L);
            builder = builder.withExpiresAt(date);
        }
        //秘钥以及加密算法
        builder = builder.withClaim("data", json);

        return builder.sign(algorithm);
    }

    /**
     * 生成会过期的token
     *
     * @param json
     * @return
     */
    public static String encodeToken(String json) {
        return encodeToken(json, null);
    }

    /**
     * 解密
     *
     * @param token
     * @return
     */
    public static TokenData decodeToken(String token) throws Exception {
        //秘钥以及加密算法
        try {
            Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer(TOKEN_ISSUER)
                    .build();
            DecodedJWT decodedJWT = verifier.verify(token);
            Map<String, Claim> map = decodedJWT.getClaims();
            String data = map.get("data").asString();

            if (decodedJWT.getExpiresAt() != null
                    && decodedJWT.getExpiresAt().getTime() < new Date().getTime()) {
                return new TokenData().setData(JSONObject.parseObject(data)).setSuccess(false)
                        .setReason("身份已过期，请重新登录");
            }
            return new TokenData().setData(JSONObject.parseObject(data)).setSuccess(true)
                    .setReason("验证通过");
        } catch (Exception e) {
            return new TokenData().setData(null).setSuccess(false).setReason(e.getMessage());
        }
    }

}
