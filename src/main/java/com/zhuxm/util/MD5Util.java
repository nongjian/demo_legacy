package com.zhuxm.util;

import org.springframework.util.DigestUtils;

/**
 * Created by IntelliJ IDEA.
 * User: zhuxm
 * Date: 2021/8/2
 * Time: 16:29
 */
public class MD5Util {

    private static final String key = "mlkh21!";

    public static String encode(String str) {
        return DigestUtils.md5DigestAsHex((str + key).getBytes());
    }

    public static Boolean check(String str, String pwd) {
        return DigestUtils.md5DigestAsHex((str + key).getBytes()).equals(pwd);
    }
}
