package com.zhuxm.user.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zhuxm.common.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * Created by IntelliJ IDEA.
 * User: zhuxm
 * Date: 2021/8/2
 * Time: 16:09
 */
@Data
@ToString
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "用户信息", value = "用户信息")
@TableName("project_user")
public class User extends BaseEntity {

    @ApiModelProperty(value = "用户名", position = 1)
    private String userName;
    @ApiModelProperty(value = "用户账号", position = 2)
    private String userAccount;
    @ApiModelProperty(value = "用户密码（加密）", position = 3)
    @JsonIgnore
    private String userPassword;

    @ApiModelProperty(value = "用户令牌", position = 4)
    private String userToken;

    @ApiModelProperty(value = "盐值", position = 5)
    private String salt;
}
