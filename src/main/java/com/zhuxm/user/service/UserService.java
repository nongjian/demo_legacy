package com.zhuxm.user.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhuxm.common.Rest;
import com.zhuxm.common.RestStatus;
import com.zhuxm.user.entity.User;
import com.zhuxm.user.mapper.UserMapper;
import com.zhuxm.util.JwtUtil;
import com.zhuxm.util.MD5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * User: zhuxm
 * Date: 2021/8/2
 * Time: 16:09
 */
@Service
public class UserService extends ServiceImpl<UserMapper, User> {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserService userService;

    public User register(User user) {
        user.setUserPassword(MD5Util.encode(user.getUserPassword()));
        synchronized (this) {
            Integer exists = userMapper.selectCount(new QueryWrapper<User>().lambda().eq(User::getUserAccount, user.getUserAccount()));
            if (exists > 0) {
                Rest.Exception(RestStatus.BAD_REQUEST, "该帐号已存在");
            }
            userMapper.insert(user);
        }
        return user;
    }

    public User login(User user) {
        User exists = userMapper.selectOne(new QueryWrapper<User>().lambda().eq(User::getUserAccount, user.getUserAccount()));
        if (exists == null) {
            Rest.Exception(RestStatus.BAD_REQUEST, "该帐号不存在");
            return null;
        }
        if (MD5Util.check(user.getUserPassword(), exists.getUserPassword())) {
            // 调用设置token的方法，并清除该token的缓存
            exists = userService.setTokenToUser(exists, JwtUtil.encodeToken(JSONObject.toJSONString(user)));
        } else {
            Rest.Exception(RestStatus.BAD_REQUEST, "密码错误，请重试");
        }
        return exists;
    }

    /**
     * 创建一个新的token的时候，清除针对这个token的缓存
     *
     * @param user
     * @param token
     * @return
     */
    @CacheEvict(value = {"userinfo"}, key = "#token")
    public User setTokenToUser(User user, String token) {
        user.setUserToken(token);
        userMapper.updateById(user);
        return user;
    }

    /**
     * 根据token缓存用户信息，可以极大的降低数据库负担并提升速度
     *
     * @param token
     * @return
     */
    @Cacheable(value = {"userinfo"}, key = "#token")
    public User getUserByToken(String token) {
        try {
            return userMapper.selectOne(new QueryWrapper<User>().lambda().eq(User::getUserToken, token));
        } catch (Exception e) {
            return null;
        }
    }
}
