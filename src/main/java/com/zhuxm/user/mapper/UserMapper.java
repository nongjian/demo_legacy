package com.zhuxm.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhuxm.user.entity.User;

/**
 * Created by IntelliJ IDEA.
 * User: zhuxm
 * Date: 2021/8/2
 * Time: 16:09
 */
public interface UserMapper extends BaseMapper<User> {
}
