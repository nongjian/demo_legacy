package com.zhuxm.user.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhuxm.common.Rest;
import com.zhuxm.common.base.UserHolder;
import com.zhuxm.user.entity.User;
import com.zhuxm.user.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by IntelliJ IDEA.
 * User: zhuxm
 * Date: 2021/8/2
 * Time: 16:09
 */
@RestController
@RequestMapping("/api/user")
@Api(value = "用户模块", tags = "用户模块")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    @ApiOperation("用户注册")
    public Rest<?> register(@RequestBody User user) {
        user = userService.register(user);
        return Rest.OK("注册成功", user);
    }

    @PostMapping("/login")
    @ApiOperation("用户登录")
    public Rest<?> login(@RequestBody User user) {
        user = userService.login(user);
        return Rest.OK("登录成功", user);
    }

    @GetMapping("/mine")
    @ApiOperation("获取当前用户信息")
    public Rest<?> getMyInfo(String token) {
        return Rest.OK(UserHolder.getUser());
    }

    @GetMapping("")
    @ApiOperation("获取用户列表")
    public Rest<?> getList() {
        return Rest.OK(userService.list(new QueryWrapper<>()));
    }

    @GetMapping("/{id}")
    @ApiOperation("查看单个用户信息")
    public Rest<?> getDetail(@PathVariable("id") Long id) {
        return Rest.OK(userService.getById(id));
    }
}
