package com.zhuxm.common.base;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import springfox.documentation.annotations.ApiIgnore;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: zhuxm
 * Date: 2021/8/2
 * Time: 15:57
 */
@Data
@ToString
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@EqualsAndHashCode
@ApiIgnore
public class BaseEntityNoLogic implements Serializable {

    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "ID", position = 1)
    private Long id;

    @ApiModelProperty(value = "创建时间", position = 101)
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @ApiModelProperty(value = "创建人ID", position = 102)
    private Long createUserId;
    @ApiModelProperty(value = "更新时间", position = 103)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    @ApiModelProperty(value = "更新人ID", position = 104)
    private Long updateUserId;
}
