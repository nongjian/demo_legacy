package com.zhuxm.common.base;

import com.zhuxm.user.entity.User;

/**
 * Created by IntelliJ IDEA.
 * User: zhuxm
 * Date: 2021/8/4
 * Time: 8:48
 */
public class UserHolder {


    private static final ThreadLocal<User> userHolder = new ThreadLocal<>();

    public static void setUser(User user) {
        userHolder.set(user);
    }

    public static User getUser() {
        User user = userHolder.get();
        if (user == null) {
            user = new User();
            user.setId(-1L);
            return user;
        }
        return userHolder.get();
    }

    public static void remove() {
        userHolder.remove();
    }
}
