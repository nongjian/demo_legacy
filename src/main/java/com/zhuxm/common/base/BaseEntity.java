package com.zhuxm.common.base;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Created by IntelliJ IDEA.
 * User: zhuxm
 * Date: 2021/8/2
 * Time: 15:59
 */
@Data
@ToString
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiIgnore
public class BaseEntity extends BaseEntityNoLogic {

    @TableLogic
    @JsonIgnore
    @ApiModelProperty(value = "是否删除", position = 106)
    @TableField(fill = FieldFill.INSERT)
    private Boolean isDeleted;
}
