package com.zhuxm.common;

import lombok.Data;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Created by IntelliJ IDEA.
 * User: zhuxm
 * Date: 2021/6/8
 * Time: 18:59
 */
@Data
@ApiIgnore
public class RestException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private RestStatus status;
    private String msg;

    public RestException(String error) {
        super(error);
        this.msg = error;
    }

    public RestException(RestStatus status, String msg) {
        this.status = status;
        this.msg = msg;
    }


}
