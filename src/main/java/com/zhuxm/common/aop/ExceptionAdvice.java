package com.zhuxm.common.aop;

import com.zhuxm.common.Rest;
import com.zhuxm.common.RestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by IntelliJ IDEA.
 * User: zhuxm
 * Date: 2021/7/8
 * Time: 22:52
 */

/**
 * 异常处理类，后端所有的异常经过处理以后再返回给前端，避免直接返回报错信息
 */
@ControllerAdvice
public class ExceptionAdvice {

    @Autowired
    private HttpServletRequest request;

    /**
     * 自动抛出的异常
     * @param ex
     * @return
     * @throws Exception
     */
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Rest<?> runtimeException(Exception ex) throws Exception {
        String result;
        // 针对数据验证注解做特别处理
        if (ex.getClass().getName().contains("org.springframework.validation") ||
                ex.getClass().getName().contains("org.springframework.context.support") ||
                ex.getClass().getName().contains("org.springframework.web.bind.MethodArgumentNotValidException") ||
                ex.getClass().getName().contains("javax.validation.constraints")) {
            result = ex.getMessage();
            result = result.substring(result.lastIndexOf(
                    "default message [") + "default message [".length(),
                    result.lastIndexOf("]") - 1);
            return Rest.BAD_REQUEST(result);
        }
        ex.printStackTrace();
        // 返回报错信息
        return Rest.ERROR(ex.getLocalizedMessage());
    }

    /**
     * 手动抛出的异常
     * @param ex
     * @return
     */
    @ExceptionHandler(RestException.class)
    @ResponseBody
    public Rest<?> XhzException(RestException ex) {
        // 如果抓到的是自定义的报错信息，就直接返回
        return ex.getStatus() == null ? Rest.ERROR(ex.getMsg()) : Rest.COMMON(ex.getStatus(), ex.getMsg());
    }
}

