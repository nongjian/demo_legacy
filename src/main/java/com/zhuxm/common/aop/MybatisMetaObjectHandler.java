package com.zhuxm.common.aop;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.zhuxm.common.base.UserHolder;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 自动注入创建时间、修改时间
 */
@Component
public class MybatisMetaObjectHandler implements MetaObjectHandler {

    private static final String IS_DELETED = "isDeleted";
    private static final String CREATE_TIME = "createTime";
    private static final String CREATE_USER_ID = "createUserId";
    private static final String UPDATE_TIME = "updateTime";
    private static final String UPDATE_USER_ID = "updateUserId";

    @Override
    public void insertFill(MetaObject metaObject) {
        this.setFieldValByName(IS_DELETED, false, metaObject);
        this.setFieldValByName(CREATE_TIME, new Date(), metaObject);
        this.setFieldValByName(CREATE_USER_ID, UserHolder.getUser().getId(), metaObject);
        this.setFieldValByName(UPDATE_TIME, new Date(), metaObject);
        this.setFieldValByName(UPDATE_USER_ID, UserHolder.getUser().getId(), metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.setFieldValByName(UPDATE_TIME, new Date(), metaObject);
        this.setFieldValByName(UPDATE_USER_ID, UserHolder.getUser().getId(), metaObject);
    }
}
