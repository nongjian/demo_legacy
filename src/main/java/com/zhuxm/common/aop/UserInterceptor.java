package com.zhuxm.common.aop;

import com.alibaba.fastjson.JSON;
import com.zhuxm.common.Rest;
import com.zhuxm.common.base.UserHolder;
import com.zhuxm.user.entity.User;
import com.zhuxm.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.boot.configurationprocessor.json.JSONStringer;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import springfox.documentation.spring.web.json.Json;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.Map;

/**
 * Create by XiaoMo
 * Date : 2019/5/7
 * Time : 17:07
 **/

/**
 * 拦截所有请求并处理
 */
@Component
public class UserInterceptor implements HandlerInterceptor {

    @Autowired
    private UserService userService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object obj) throws Exception {
        // 取出token
        String token = request.getHeader("token");
        User user = null;
        if (token != null) {
            // 根据token查出用户信息
            user = userService.getUserByToken(token);
        }
        // 设置当前用户
        UserHolder.setUser(user);
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex) throws Exception {
        // 清除ThreadLocal中的信息，防止内存溢出
        UserHolder.remove();
    }

    /**
     * 返回json数据
     *
     * @param response
     * @param r
     */
    private void returnJson(HttpServletResponse response, Rest r) {
        PrintWriter writer = null;
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=UTF-8");
        try {
            writer = response.getWriter();
            writer.print(JSON.toJSONString(r));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null)
                writer.close();
        }
    }
}
