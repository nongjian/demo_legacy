package com.zhuxm.shiro.realm;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;

/**
 *
 * @Description
 * @Author andy
 * @Date 2021/11/26 14:20
 * @param
 * @return
 *
 **/
public class CustomerMD5Realm extends AuthorizingRealm {
    //    授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        return null;
    }

    //认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        //在token中获取用户名
        String principal = authenticationToken.getPrincipal().toString();
        System.out.println("用户名为="+principal);
        //根据身份信息使用jdbc mybatis redis查询相关数据库
        if("danong".equals(principal)){
            //参数1：返回数据库正确的用户名 参数2：返回数据库中正确的密码 参数3：返回当前realm的名字 this.getName()

//            return new SimpleAuthenticationInfo(principal,"e10adc3949ba59abbe56e057f20f883e",this.getName());
            //返回随即盐
            return new SimpleAuthenticationInfo(
                    principal,
                    "e10adc3949ba59abbe56e057f20f883e",
                    ByteSource.Util.bytes("salt"),
                    this.getName());
        }
        return null;
    }

}
