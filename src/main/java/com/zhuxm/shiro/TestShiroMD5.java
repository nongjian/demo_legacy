package com.zhuxm.shiro;

import org.apache.shiro.crypto.hash.Md5Hash;

public class TestShiroMD5 {
    public static void main(String[] args) {
        //使用MD5
        Md5Hash md5Hash = new Md5Hash("123456");
        System.out.println(md5Hash.toHex());
        //使用MD5 + salt处理
        Md5Hash md5Hash2 = new Md5Hash("123456","salt");
        System.out.println(md5Hash2.toHex());
        //使用MD5 + salt + hash处理
        Md5Hash md5Hash3 = new Md5Hash("123456","salt",2048);
        System.out.println(md5Hash3.toHex());
    }
}
