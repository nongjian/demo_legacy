//package com.zhuxm.shiro;
//
//import com.zhuxm.shiro.realm.CustomerRealm;
//import org.apache.shiro.SecurityUtils;
//import org.apache.shiro.authc.IncorrectCredentialsException;
//import org.apache.shiro.authc.UnknownAccountException;
//import org.apache.shiro.authc.UsernamePasswordToken;
//import org.apache.shiro.mgt.DefaultSecurityManager;
//import org.apache.shiro.subject.Subject;
//
///**
// *
// * @Description  使用自定义realm
// * @Author andy
// * @Date 2021/11/26 10:53
// * @param
// * @return
// *认证：
// *
// * 1.最终执行用户名比较是 在SimpleAccountRealm类 的 doGetAuthenticationInfo 方法中完成用户名校验
// *
// * 2.最终密码校验是在 AuthenticatingRealm类 的 assertCredentialsMatch方法 中
// *
// * 总结：
// *
// * AuthenticatingRealm 认证realm doGetAuthenticationInf
// *
// * AuthorizingRealm 授权realm doGetAuthorizationInfo
// *
// * 自定义Realm的作用：放弃使用.ini文件，使用数据库查询
// **/
//public class TestCustomerRealmAuthenticator {
//    public static void main(String[] args) {
//        //创建securitymanager
//        DefaultSecurityManager defaultSecurityManager = new DefaultSecurityManager();
//        //设置滴定仪realm
//        defaultSecurityManager.setRealm(new CustomerRealm());
//        //将安全工具类设置安全工具类
//        SecurityUtils.setSecurityManager(defaultSecurityManager);
//        Subject subject = SecurityUtils.getSubject();
//        //创建token
//        UsernamePasswordToken usernamePasswordToken =new UsernamePasswordToken("danong","123456");
//        try {
//            System.out.println("认证状态" + subject.isAuthenticated());
//            subject.login(usernamePasswordToken);//用户认证
//            System.out.println("认证状态" + subject.isAuthenticated());
//        } catch (UnknownAccountException e) {
//            e.printStackTrace();
//            System.out.println("认证失败：用户名不存在");
//        } catch (IncorrectCredentialsException e) {
//            e.printStackTrace();
//            System.out.println("认证失败：密码错误");
//        }
//    }
//}
