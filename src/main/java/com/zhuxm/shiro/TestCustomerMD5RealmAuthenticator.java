package com.zhuxm.shiro;

import com.zhuxm.shiro.realm.CustomerMD5Realm;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.subject.Subject;

/**
 *
 * @Description  使用自定义realm
 * @Author andy
 * @Date 2021/11/26 10:53
 * @param
 * @return
 *MD5
 **/
public class TestCustomerMD5RealmAuthenticator {
    public static void main(String[] args) {
        //创建securitymanager
        DefaultSecurityManager defaultSecurityManager = new DefaultSecurityManager();
        //设置自定义realm
        CustomerMD5Realm realm = new CustomerMD5Realm();
        //设置realm使用hash凭证匹配器
        HashedCredentialsMatcher credentialsMatcher = new HashedCredentialsMatcher();
        credentialsMatcher.setHashAlgorithmName("md5");
        realm.setCredentialsMatcher(credentialsMatcher);

        //将安全工具类设置安全工具类
        SecurityUtils.setSecurityManager(defaultSecurityManager);
        Subject subject = SecurityUtils.getSubject();
        //创建token
        UsernamePasswordToken usernamePasswordToken =new UsernamePasswordToken("danong","123456");
        try {
            System.out.println("认证状态" + subject.isAuthenticated());
            subject.login(usernamePasswordToken);//用户认证
            System.out.println("认证状态" + subject.isAuthenticated());
        } catch (UnknownAccountException e) {
            e.printStackTrace();
            System.out.println("认证失败：用户名不存在");
        } catch (IncorrectCredentialsException e) {
            e.printStackTrace();
            System.out.println("认证失败：密码错误");
        }
    }
}
